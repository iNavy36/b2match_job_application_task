import React, { useState, useEffect } from "react";
import CalendarComponent from "../components/CalendarComponent";

interface commit {
  author_email: string;
  author_name: string;
  authored_date: string;
  committed_date: string;
  committer_email: string;
  committer_name: string;
  created_at: string;
  id: string;
  message: string;
  parent_ids: string[];
  short_id: string;
  title: string;
  trailers: object;
  web_url: string;
}
interface Props {
  getData: (data: commit[]) => void;
}

function Api({ getData }: Props) {
  const url = "https://gitlab.com/api/v4/projects/44887573/repository/commits";
  const [data, setData] = useState<commit[] | never[]>([]);
  const fetchInfo = async () => {
    const res = await fetch(url);
    const d = await res.json();
    setData(d);
  };
  useEffect(() => {
    fetchInfo();
  }, []);

  useEffect(() => {
    getData(data);
  }, [data]);

  if (data !== undefined) {
    return <CalendarComponent commits={data} />;
  } else {
    return <CalendarComponent commits={[]} />;
  }
}

export default Api;
