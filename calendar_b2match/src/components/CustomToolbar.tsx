import React from "react";

export class CustomToolbar extends React.Component {
  render() {
    return (
      <div className="rbc-toolbar">
        <span className="rbc-btn-group">
          <button type="button" onClick={() => this.navigate("PREV")}>
            {"<"}
          </button>
        </span>
        <span className="rbc-btn-group pull-right">
          <button type="button" onClick={() => this.navigate("TODAY")}>
            Show today
          </button>
        </span>
        <span className="rbc-toolbar-label">{this.props.label}</span>
        <span className="rbc-btn-group">
          <button type="button" onClick={() => this.navigate("NEXT")}>
            {">"}
          </button>
        </span>
      </div>
    );
  }

  navigate = (action) => {
    this.props.onNavigate(action);
  };
}
