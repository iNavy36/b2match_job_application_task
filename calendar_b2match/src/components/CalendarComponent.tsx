import React, { useState, useEffect } from "react";
import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import format from "date-fns/format";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import getDay from "date-fns/getDay";
import "react-big-calendar/lib/css/react-big-calendar.css";
import hr from "date-fns/locale/hr";
import { CustomToolbar } from "./CustomToolbar";
import "../assets/calendar.css";

const locales = {
  hr: hr,
};
const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

interface event {
  title: string;
  start: Date;
  end: Date;
  author_name: string;
  short_id: string;
  message: string;
}

interface commit {
  author_email: string;
  author_name: string;
  authored_date: string;
  committed_date: string;
  committer_email: string;
  committer_name: string;
  created_at: string;
  id: string;
  message: string;
  parent_ids: string[];
  short_id: string;
  title: string;
  trailers: object;
  web_url: string;
}

interface Props {
  commits: commit[];
}

export default function CalendarComponent({ commits }: Props) {
  const [data, setData] = useState<commit[] | never[]>([]);
  useEffect(() => {
    setData(commits);
  }, [commits]);

  const [events, setEvents] = useState<event[]>([]);
  useEffect(() => {
    setEvents(
      data.map((oneCommit) => ({
        title: oneCommit.title,
        start: new Date(oneCommit.created_at),
        end: new Date(oneCommit.committed_date),
        author_name: oneCommit.author_name,
        short_id: oneCommit.short_id,
        message: oneCommit.message,
      }))
    );
  }, [data]);
  useEffect(() => {
    console.log(events);
  }, [events]);

  const [selected, setSelected] = useState<event>();

  const handleSelected = (event: event) => {
    setSelected(event);
    alert(
      "Title: " +
        event.title +
        "\nAuthor: " +
        event.author_name +
        "\nCommit message: " +
        event.message +
        "\nDate: " +
        event.start.getDate() +
        ". " +
        event.start.toLocaleString("default", { month: "long" }) +
        " " +
        event.start.getFullYear() +
        "."
    );
  };

  return (
    <Calendar
      localizer={localizer}
      events={events}
      startAccessor="start"
      endAccessor="end"
      components={{
        toolbar: CustomToolbar,
      }}
      onSelectEvent={handleSelected}
      views={{ month: true }}
      style={{ height: 550, width: "90%", margin: "2%" }}
    />
  );
}
