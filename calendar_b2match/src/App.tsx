import React, { useEffect, useState } from "react";
import Api from "./assets/API";
interface commit {
  author_email: string;
  author_name: string;
  authored_date: string;
  committed_date: string;
  committer_email: string;
  committer_name: string;
  created_at: string;
  id: string;
  message: string;
  parent_ids: string[];
  short_id: string;
  title: string;
  trailers: object;
  web_url: string;
}

export default function App() {
  const [data, setData] = useState<commit[] | never[]>([]);
  const [name, setName] = useState("");
  function getData(data: commit[]) {
    setData(data);
  }

  useEffect(() => {
    if (data.length > 0) {
      var splitted = data[0].web_url.split("/");
      setName(splitted[4]);
    }
  }, [data]);

  const noData = () => {
    return (
      data.length == 0 && (
        <header>
          <h1>Loading</h1>
        </header>
      )
    );
  };
  const dataPresent = () => {
    return (
      data.length > 0 && (
        <header>
          <h1>Commits for {name}</h1>
        </header>
      )
    );
  };
  return (
    <>
      {noData()}
      {dataPresent()}
      <Api getData={getData} />
    </>
  );
}

/*
      {data.map((commit, index) => {
        <p className={index}>{commit.title}</p>;
      })}
*/
